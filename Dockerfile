FROM openjdk:11-jdk-alpine

ARG FILE_PATH

COPY $FILE_PATH /app.jar

EXPOSE 8080
ENTRYPOINT ["sh", "java -jar app.jar"]
CMD ["sh"]
