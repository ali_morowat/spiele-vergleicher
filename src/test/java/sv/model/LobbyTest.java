package sv.model;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class LobbyTest {

    @Test
    public void shouldCreateLobbyWithNewUuid() {
        var lobby1 = new Lobby();
        var lobby2 = new Lobby();

        assertNotEquals(lobby1.getUuid(), lobby2.getUuid());
    }

    @Test
    public void shouldAddNewUser() {
        var user = new User("username");
        var lobby = new Lobby();

        assertFalse(lobby.containsUser(user.getUuid()));
        assertTrue(lobby.add(user));
        assertTrue(lobby.containsUser(user.getUuid()));
    }

    @Test
    public void shouldNotAddExistingUser() {
        var user = new User("username");
        var lobby = new Lobby();

        assertFalse(lobby.containsUser(user.getUuid()));
        assertTrue(lobby.add(user));
        assertEquals(1, lobby.getAllUsers().size());

        assertFalse(lobby.add(user));
        assertTrue(lobby.containsUser(user.getUuid()));
        assertEquals(1, lobby.getAllUsers().size());
    }

    @Test
    public void shouldThrowErrorWhenAddingNull() {
        var lobby = new Lobby();

        var exception = assertThrows(NullPointerException.class, () -> lobby.add(null));
        assertEquals("The provided user is null and can not be added to a lobby!", exception.getMessage());
    }

    @Test
    public void shouldReturnAllUsers() {
        var lobby = new Lobby();
        var user = new User("username");

        lobby.add(user);
        var allsUsers = lobby.getAllUsers();
        assertEquals(1, allsUsers.size());
        assertEquals(user, allsUsers.get(0));
    }

    @Test
    public void shouldGetUserById() {
        var lobby = new Lobby();
        var user = new User("username");

        lobby.add(user);
        var extractedUser = lobby.getUserById(user.getUuid());

        assertEquals(user, extractedUser);
    }
}