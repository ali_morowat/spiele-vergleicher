package sv.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;

import com.google.gson.Gson;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import sv.model.Lobby;
import sv.model.User;
import sv.service.LobbyService;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class LobbyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LobbyService mockLobbyService;

    @BeforeEach
    public void setup() {
        reset(mockLobbyService);
    }

    @Test
    public void shouldCreateNewLobbyAndReturnTheId() throws Exception {
        ArgumentCaptor<Lobby> lobbyCaptor = ArgumentCaptor.forClass(Lobby.class);
        when(mockLobbyService.add(any(Lobby.class))).thenReturn(true);

        var result = mockMvc.perform(MockMvcRequestBuilders.get("/api/lobby/").accept(MediaType.APPLICATION_JSON))
                .andReturn();
        assertEquals(HttpStatus.CREATED.value(), result.getResponse().getStatus());

        var uuidResult = new ObjectMapper().readValue(result.getResponse().getContentAsString(), UUID.class);

        verify(mockLobbyService, times(1)).add(lobbyCaptor.capture());

        assertEquals(lobbyCaptor.getValue().getUuid(), uuidResult);
    }

    @Test
    public void shouldJoinLobbyAndReturnUserId() throws Exception {
        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        var username = "username";
        var uuid = UUID.randomUUID();

        var mockLobby = mock(Lobby.class);
        when(mockLobby.add(any())).thenReturn(true);

        when(mockLobbyService.findBy(uuid)).thenReturn(mockLobby);
        when(mockLobbyService.contains(uuid)).thenReturn(true);

        var result = mockMvc.perform(
                MockMvcRequestBuilders.post("/api/lobby/" + uuid).content(username).accept(MediaType.APPLICATION_JSON))
                .andReturn();
        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());

        var uuidResult = new ObjectMapper().readValue(result.getResponse().getContentAsString(), UUID.class);

        verify(mockLobby).add(userCaptor.capture());

        assertEquals(userCaptor.getValue().getUuid(), uuidResult);
    }

    @Test
    public void shouldNotJoinLobbyAndReturnError() throws Exception {
        var username = "username";
        var uuid = UUID.randomUUID();


        when(mockLobbyService.contains(uuid)).thenReturn(false);

        var result = mockMvc.perform(
                MockMvcRequestBuilders.post("/api/lobby/" + uuid).content(username).accept(MediaType.APPLICATION_JSON))
                .andReturn();
        assertEquals(HttpStatus.NOT_FOUND.value(), result.getResponse().getStatus());
    }

    @Test
    public void shouldPutSuggestionList() throws Exception {
        var lobbyUuid = UUID.randomUUID();
        var user = new User("username");
        var mockLobby = mock(Lobby.class);
        when(mockLobby.getUuid()).thenReturn(lobbyUuid);
        when(mockLobby.containsUser(user.getUuid())).thenReturn(true);
        when(mockLobby.getUserById(user.getUuid())).thenReturn(user);

        when(mockLobbyService.contains(mockLobby.getUuid())).thenReturn(true);
        when(mockLobbyService.findBy(mockLobby.getUuid())).thenReturn(mockLobby);

        var response = mockMvc.perform(
                MockMvcRequestBuilders.put(new URI(String.format("/api/lobby/%s/user/%s/list", mockLobby.getUuid(), user.getUuid().toString())))
                        .content("[\"foo\", \"bar\"]").header("Content-Type", "application/json").accept(MediaType.APPLICATION_JSON))
                .andReturn();
        assertEquals(HttpStatus.OK.value(), response.getResponse().getStatus());
    }

    @Test
    public void shouldPutSuggestionWithWrongLobbyId() throws Exception {
        var lobbyUuid = UUID.randomUUID();

        when(mockLobbyService.contains(any())).thenReturn(false);

        var response = mockMvc.perform(
                MockMvcRequestBuilders.put(new URI(String.format("/api/lobby/%s/user/%s/list", lobbyUuid, UUID.randomUUID())))
                        .content("[\"foo\", \"bar\"]").header("Content-Type", "application/json").accept(MediaType.APPLICATION_JSON))
                .andReturn();
        assertEquals(HttpStatus.NOT_FOUND.value(), response.getResponse().getStatus());
    }

    public void shouldPutSuggestionListWithWrongUserId() throws Exception {
        var lobbyUuid = UUID.randomUUID();
        var mockLobby = mock(Lobby.class);
        when(mockLobby.containsUser(any())).thenReturn(false);

        when(mockLobbyService.contains(mockLobby.getUuid())).thenReturn(true);
        when(mockLobbyService.findBy(mockLobby.getUuid())).thenReturn(mockLobby);

        var response = mockMvc.perform(
                MockMvcRequestBuilders.put(new URI(String.format("/api/lobby/%s/user/%s/list", lobbyUuid, UUID.randomUUID())))
                        .content("[\"foo\", \"bar\"]").header("Content-Type", "application/json").accept(MediaType.APPLICATION_JSON))
                .andReturn();
        assertEquals(HttpStatus.NOT_FOUND.value(), response.getResponse().getStatus());
    }

    @Test
    public void shouldGetAllUsersFromLobby() throws Exception {
        var uuid = UUID.randomUUID();
        var mockLobby = mock(Lobby.class);
        var users = getThreeUsers();
        when(mockLobby.getAllUsers()).thenReturn(ImmutableList.copyOf(users));

        when(mockLobbyService.findBy(any())).thenReturn(mockLobby);
        when(mockLobbyService.contains(uuid)).thenReturn(true);

        var result = mockMvc
                .perform(MockMvcRequestBuilders.get("/api/lobby/" + uuid + "/users").accept(MediaType.APPLICATION_JSON))
                .andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());

        System.out.println(result.getResponse().getContentAsString());

        var usersResult = new Gson().fromJson(result.getResponse().getContentAsString(),
                User[].class);
        assertEquals(3, usersResult.length);

    }

    @Test
    public void shouldNotFoundGetAllUsersFromLobby() throws Exception {
        var uuid = UUID.randomUUID();

        var result = mockMvc
                .perform(MockMvcRequestBuilders.get("/api/lobby/" + uuid + "/users").accept(MediaType.APPLICATION_JSON))
                .andReturn();

        assertEquals(HttpStatus.NOT_FOUND.value(), result.getResponse().getStatus());
    }

    private List<User> getThreeUsers() {
        var usernameList = List.of("username1", "username2", "username3");
        var userList = new ArrayList<User>();
        usernameList.forEach(username -> userList.add(new User(username)));
        return userList;
    }

}