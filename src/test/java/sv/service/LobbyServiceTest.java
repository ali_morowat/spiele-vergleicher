package sv.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Test;

import sv.model.Lobby;

public class LobbyServiceTest {

   @Test
   public void shouldAddOnlyOnesLobby() {
      var lobbyService = new LobbyService();
      var lobby = new Lobby();

      assertTrue(lobbyService.add(lobby));
      assertFalse(lobbyService.add(lobby));
   }

   @Test
   public void shouldContainLobby() {
      var lobbyService = new LobbyService();
      var lobby = new Lobby();

      lobbyService.add(lobby);

      assertTrue(lobbyService.contains(lobby.getUuid()));
   }

   @Test
   public void shouldFindLobbies() {
      var lobbyService = new LobbyService();
      var lobby = new Lobby();

      lobbyService.add(lobby);

      assertEquals(lobby, lobbyService.findBy(lobby.getUuid()));
   }

   @Test
   public void shouldRemove() {
      var lobbyService = new LobbyService();
      var lobby = new Lobby();

      lobbyService.add(lobby);
      assertTrue(lobbyService.contains(lobby.getUuid()));

      lobbyService.remove(lobby.getUuid());
      assertFalse(lobbyService.contains(lobby.getUuid()));
   }

   @Test
   public void shouldNotThrowExceptionIfTryToRemoveItTwise() {
      var lobbyService = new LobbyService();
      var lobby = new Lobby();

      lobbyService.add(lobby);
      assertTrue(lobbyService.contains(lobby.getUuid()));

      lobbyService.remove(lobby.getUuid());
      lobbyService.remove(lobby.getUuid());
   }
}