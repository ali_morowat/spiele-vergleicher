package sv.service;

import org.springframework.stereotype.Service;

import sv.model.Lobby;

import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Service
public class LobbyService {

    private final Map<UUID, Lobby> lobbies = new ConcurrentHashMap<UUID, Lobby>();

    public boolean contains(UUID uuid) {
        return lobbies.containsKey(uuid);
    }

    public Lobby findBy(UUID uuid) {
        return lobbies.get(uuid);
    }

    public boolean add(Lobby lobby) {
        Objects.requireNonNull(lobby, "The lobby is null and this is not allowed");
        if (lobbies.containsKey(lobby.getUuid()))
            return false;
        lobbies.put(lobby.getUuid(), lobby);
        return true;
    }

    public void remove(UUID uuid) {
        if (!lobbies.containsKey(uuid))
            return;
        lobbies.remove(uuid);
    }

}