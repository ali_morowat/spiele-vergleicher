package sv.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;
import sv.model.Lobby;
import sv.model.User;
import sv.service.LobbyService;

@Log4j2
@RestController()
@RequestMapping("/api/lobby")
@CrossOrigin(origins = "*") // fürs testen
public class LobbyController {

    private final LobbyService lobbyService;

    public LobbyController(LobbyService lobbyService) {
        this.lobbyService = lobbyService;
    }

    @GetMapping()
    public ResponseEntity<UUID> createLobby() {
        Lobby lobby = new Lobby();
        lobbyService.add(lobby);
        log.info(String.format("Creating lobby: %s", lobby.getUuid()));

        return new ResponseEntity<>(lobby.getUuid(), HttpStatus.CREATED);
    }

    @PostMapping("{lobby_id}")
    public ResponseEntity<UUID> joinLobby(@RequestBody String username, @PathVariable("lobby_id") UUID lobbyId) {
        if (!lobbyService.contains(lobbyId))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        Lobby lobby = lobbyService.findBy(lobbyId);
        User user = new User(username);
        lobby.add(user);

        return new ResponseEntity<>(user.getUuid(), HttpStatus.OK);
    }

    @GetMapping("{lobby_id}/users")
    public ResponseEntity<List<User>> getAllUsers(@PathVariable("lobby_id") UUID lobbyId) {
        if (!lobbyService.contains(lobbyId))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        Lobby lobby = lobbyService.findBy(lobbyId);
        var users = lobby.getAllUsers();

        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @PutMapping("{lobby_id}/user/{user_id}/list")
    public ResponseEntity<?> putSuggestionList(@RequestBody List<String> suggestionList,
            @PathVariable("lobby_id") UUID lobbyId, @PathVariable("user_id") UUID userId) {

        if (!lobbyService.contains(lobbyId))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        Lobby lobby = lobbyService.findBy(lobbyId);

        if (lobby.containsUser(userId)) {
            User user = lobby.getUserById(userId);
            user.setGames(suggestionList);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}