package sv.controller;

import java.net.http.WebSocket;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;

import springfox.documentation.spring.web.json.Json;
import sv.model.*;
import sv.service.LobbyService;

@Log4j2
@Controller
public class LobbyWebSocketController {

    private final LobbyService lobbyService;
    private final SimpMessagingTemplate simpMessagingTemplate;

    private List<PingUser> pingList = new ArrayList<>(); // user that send a ping
    private List<PingUser> toRemoveFromPing = new ArrayList<>(); // user that didn't responded in time with a ping

    public LobbyWebSocketController(LobbyService lobbyService, SimpMessagingTemplate simpMessagingTemplate) {
        this.lobbyService = lobbyService;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @MessageMapping("/{lobby_id}")
    @SendTo("/r/{lobby_id}")
    public WebSocketMessage lobbyEvent(@DestinationVariable("lobby_id") UUID lobbyId, WebSocketMessage message){
        //log.info("Recieving Text Message: " + message);
        //WebSocketMessage message = new WebSocketMessage(lobbyId, Event.CONNECT, "test");

        switch(message.getEvent()) {
            case CONNECT:
                return message;
            case READY_STATE_CHANGE:
                Lobby lobby = lobbyService.findBy(lobbyId);
                if(!lobby.containsUser(message.getUserUuid())) {
                    return new WebSocketMessage(message.getUserUuid(), Event.ERROR, "Errorcode TS12: Please refer to the manuel");
                }
                User user = lobby.getUserById(message.getUserUuid());
                user.setReady(!user.isReady());

                return new WebSocketMessage(message.getUserUuid(), Event.READY_STATE_CHANGE, ""+user.isReady());
            case DISCONNECT:
                return message;
            case ERROR:
                break;
            default:
                throw new RuntimeException("The event given in the ws message was not of a valid event type");
        }

        return message;
    }

    @MessageMapping("/{lobby_id}/{user_id}/ping")
    @SendTo("/r/{user_id}")
    public String ping(@DestinationVariable("lobby_id") UUID lobbyId, @DestinationVariable("user_id") UUID userId, String ping){

        boolean userInList = false;
        PingUser pingUser = new PingUser(userId, lobbyId, Instant.now());

        for(PingUser user : pingList) {
            if(user.getUserId().equals(userId)) {
                userInList = true;
                pingUser = user;
            }
        }

        if(userInList){
            pingUser.setLastPing(Instant.now());
        } else {
            pingList.add(pingUser);
        }

        return "pong";
    }

    @Scheduled(fixedRate = 10000)
    public void checkIfUserIsActive() {
        for(PingUser user : pingList) {
            long diff = Duration.between(user.getLastPing(), Instant.now()).toMillis();
            if(diff >= 10000) {
                // the user didn't respond withing 5 sec
                toRemoveFromPing.add(user);

                WebSocketMessage message = new WebSocketMessage(user.getUserId(), Event.DISCONNECT, "User Timed out");
                this.simpMessagingTemplate.convertAndSend(String.format("/r/%s", user.getLobbyId()), message);
            }
        }

        pingList.removeAll(toRemoveFromPing);
    }
}