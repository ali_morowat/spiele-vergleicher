package sv.model;

import java.util.UUID;

import lombok.Data;

@Data
public class WebSocketMessage {

    private UUID userUuid;
    private Event event;
    private String payload;

    public WebSocketMessage() {}

    public WebSocketMessage(UUID userUuid, Event event, String payload) {
        this.userUuid = userUuid;
        this.event = event;
        this.payload = payload;
    }
}