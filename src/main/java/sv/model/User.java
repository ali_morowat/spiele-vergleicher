package sv.model;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

import lombok.Data;

@Data
public class User {

    private final String username;
    private final UUID uuid;
    private boolean ready;

    private final List<String> games;

    public User(String username) {
        this.username = username;
        this.uuid = UUID.randomUUID();
        this.ready = false;
        this.games = new CopyOnWriteArrayList<String>();
    }

    public void setGames(List<String> gameList) {
        games.clear();
        games.addAll(gameList);
    }

}
