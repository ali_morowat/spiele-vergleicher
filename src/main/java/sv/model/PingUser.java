package sv.model;

import lombok.Data;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;


@Data
public class PingUser {
    private final UUID userId;
    private final UUID lobbyId;
    private Instant lastPing;

    public PingUser(UUID userId, UUID lobbyId, Instant lastPing) {
        this.userId = userId;
        this.lastPing = lastPing;
        this.lobbyId = lobbyId;
    }
}
